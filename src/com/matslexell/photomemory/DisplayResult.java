package com.matslexell.photomemory;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;
/**
 * This activity is used for displaying a result when the game is done.
 * @author matslexell
 *
 */
public class DisplayResult extends  ActionBarActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result_activity);
		String result= getIntent().getExtras().getString("result");
        getActionBar().setDisplayHomeAsUpEnabled(true);

		TextView header = (TextView) findViewById(R.id.result_activity_header);
		TextView text = (TextView) findViewById(R.id.result_activity_text);
		
		header.setText("Well Played!");
		
		text.setText("Result is following:\n\n" + result);
		
		

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == android.R.id.home) {
	    	 finish();
	    	 return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
