package com.matslexell.photomemory;

import java.io.File;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the main activity of the application "PhotoMemory"
 * 
 * @author matslexell
 * 
 */
public class MainActivity extends ActionBarActivity {

	// Setup some working folders used in the application
	public static final File workingFolder = setupFolders();
	public static final File tempFolder = new File(workingFolder
			+ File.separator + "Temp");
	public static final File picTemp = new File(workingFolder + File.separator
			+ "pictures");
	public static final File turnTemp = new File(workingFolder + File.separator
			+ "turnTable");
	public static final File pathTemp = new File(workingFolder + File.separator
			+ "path");
	public static final File pathSavedPics = new File(workingFolder
			+ File.separator + "Saved");

	public final static int MAX_PLAYERS = 4;
	public final static int MAX_PICTURES = 20;

	// Result variables
	private final int GAME_SETUP = 1;
	private final int GAME_RESULT = 2;
	private final int VIEWED_RESULT = 3;
	private final int CREATE_SET = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView text = (TextView) findViewById(R.id.welcome);
		text.setText("Welcome to photomemory!");
		Button startNew = (Button) findViewById(R.id.start_new);
		startNew.setText("Start New Game");

		// When new game starts, call game setup!
		startNew.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// If the folder has been deleted, recreate it
				if (!tempFolder.exists())
					setupFolders();
				Intent intent = new Intent(MainActivity.this, GameSetup.class);
				startActivityForResult(intent, GAME_SETUP);
			}

		});

		
		Button resumePrev = (Button) findViewById(R.id.resume_prev);
		resumePrev.setText("Resume Previous Game");

		//When pressing this button resume previously stoped game.
		resumePrev.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Load
				if (pathTemp.exists() && turnTemp.exists() && picTemp.exists()) {

					String picData = new TextIO(picTemp.getPath())
							.getEntireText();
					String turnData = new TextIO(turnTemp.getPath())
							.getEntireText();
					String pathData = new TextIO(pathTemp.getPath())
							.getEntireText();

					// If the pictures has been deleted, the game cannot be
					// resumed
					if (!new File(pathData).exists()) {
						print("The pictures used in this game has been deleted!\nPlease start new game instead...");
						return;
					}

					GameSetupCont g = new GameSetupCont(new Pictures(picData),
							new TurnTable(turnData), pathData);
					Intent intent = new Intent(MainActivity.this,
							GameActivity.class);
					// print(g.toString());
					intent.putExtra("gamesetup", g);
					startActivityForResult(intent, GAME_RESULT);
				} else {
					print("There is no previous game to open!");
				}
			}

		});

	}

	/**
	 * Create all folders being used in this application
	 * 
	 * @return
	 */
	private static File setupFolders() {
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"PhotoMemory");

		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(mediaStorageDir.getAbsolutePath(),
						"failed to create directory");
				return null;
			}
		}

		// Also create a temp folder
		File tempFolder = new File(mediaStorageDir + File.separator + "Temp");

		if (!tempFolder.exists()) {
			if (!tempFolder.mkdirs()) {
				Log.d(tempFolder.getAbsolutePath(),
						"failed to create directory");
				return null;
			}
		}

		// Also create folder for saved pictures
		File savedPicFolder = new File(mediaStorageDir + File.separator
				+ "Saved");
		if (!savedPicFolder.exists()) {
			if (!savedPicFolder.mkdirs()) {
				Log.d(savedPicFolder.getAbsolutePath(),
						"failed to create directory");
				return null;
			}
		}

		return mediaStorageDir;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * All the results from each started activity gets reported into this method
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			// Game setup has been done appropriately
			case GAME_SETUP: {
				GameSetupCont g = (GameSetupCont) data
						.getParcelableExtra("gamesetup");
				Intent intent = new Intent(MainActivity.this,
						GameActivity.class);
				intent.putExtra("gamesetup", g);
				startActivityForResult(intent, GAME_RESULT);
			}
				break;
			// Game over, report result:
			case GAME_RESULT: {
				Intent intent = new Intent(MainActivity.this,
						DisplayResult.class);
				String result = data.getStringExtra("result");
				intent.putExtra("result", result);
				startActivityForResult(intent, VIEWED_RESULT);
				deleteTempFiles();
				break;
			}
			case CREATE_SET: {
				String name = data.getStringExtra("name");
				print("Set \"" + name + "\" successfully created");
				break;
			}

			case VIEWED_RESULT:
				deleteTempFiles();
				break;
			}
		} else if (resultCode == RESULT_CANCELED) {
			switch (requestCode) {
			case CREATE_SET:
				print("No set created...");
			}
		}
	}

	/**
	 * Removes all the temporary files (saves to use on resume)
	 */
	private void deleteTempFiles() {
		picTemp.delete();
		turnTemp.delete();
		pathTemp.delete();
		delete(tempFolder);
	}

	/**
	 * For all the actionbar items
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.about:
			// Read about the game
			startActivity(new Intent(MainActivity.this, About.class));

			return true;

		case R.id.createSet:
			// Create a new picture set
			if (!pathSavedPics.exists())
				setupFolders();
			startActivityForResult(new Intent(MainActivity.this,
					CreateSet.class), CREATE_SET);
			return true;

		case R.id.deleteAll:
			
			dialog("Are you sure you want to delete all custom picture sets and data in the application folder?\n\nIMPORTANT: Any personal files in the application data folder will then also be deleted (App folder: " + workingFolder + ")",
					deleteAllListener());

			return true;

		case R.id.deleteSet:
			if (!MainActivity.pathSavedPics.exists())
				setupFolders();
			deleteSet(findViewById(R.id.settings));
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Static function to recursively delete a folder and all it's contents
	 * 
	 * @param file
	 */
	public static void delete(File file) {
		if (file.isDirectory())
			for (File subFile : file.listFiles()) {
				delete(subFile);
			}
		file.delete();
	}

	/**
	 * Start a yes or no dialog with the specified listener and message
	 * 
	 * @param message
	 * @param listener
	 */
	public void dialog(String message, DialogInterface.OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(message).setPositiveButton("Yes", listener)
				.setNegativeButton("No", listener).show();
	}

	/**
	 * When pressing an option with this listener all data files and folders
	 * that this application created gets deleted.
	 * 
	 * @return
	 */
	public DialogInterface.OnClickListener deleteAllListener() {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					MainActivity.delete(MainActivity.workingFolder);
					print("All pictures and files deleted");
				}

			}

		};
	}

	/**
	 * This listener is for deleting specific sets
	 * 
	 * @param file
	 * @return
	 */
	public DialogInterface.OnClickListener deleteSetListener(final File file) {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == DialogInterface.BUTTON_POSITIVE) {
					MainActivity.delete(file);
				}

			}

		};
	}

	/**
	 * Method that show a popup meny with all the custom picture sets. When
	 * pressing on one of them it gets deleted
	 * 
	 * @param arg0
	 */
	private void deleteSet(View arg0) {
		PopupMenu popup = new PopupMenu(this, arg0);
		popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());

		final File files[] = MainActivity.pathSavedPics.listFiles();

		if (files.length == 0) {
			print("There are no saved picture sets");
			return;
		}

		//Add all strings to popup menu
		for (int i = 0; i < files.length; i++) 
			popup.getMenu().add(0, i, 0, files[i].getName());
		

		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//Ask user if sure
				dialog("Are you sure you want to delete this set? ("
						+ files[item.getItemId()].getName() + ")",
						deleteSetListener(files[item.getItemId()]));
				return true;
			}

		});

		popup.show();
	}

	public void print(String s) {
		final Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
	}

}
