package com.matslexell.photomemory;

import java.io.File;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class handles setting up a new game.
 * 
 * @author matslexell
 * 
 */
public class GameSetup extends ActionBarActivity implements
		OnSeekBarChangeListener {
	private final int TAKEPICRESULT = 0;
	private int numPlayers = 1;
	private int numPictures = 2;
	private String path = standardpath();

	private SeekBar numPicsBar;
	private RadioButton takeNew;

	private boolean useSaved = false;

	/**
	 * In this class the user is presented with a set of options: How many
	 * pictures is going to be used? How many players? Which pictures are going
	 * to be used?
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gamesetup_activity);
		if (savedInstanceState != null) {
			numPlayers = savedInstanceState.getInt("numPlayers");
			numPictures = savedInstanceState.getInt("numPictures");
			path = savedInstanceState.getString("path");
			useSaved = savedInstanceState.getBoolean("useSaved");
		}

		SeekBar numPlayersBar = (SeekBar) findViewById(R.id.num_players_bar);

		numPicsBar = (SeekBar) findViewById(R.id.num_pictures_bar);
		numPicsBar.setOnSeekBarChangeListener(this);
		numPlayersBar.setOnSeekBarChangeListener(this);

		((TextView) findViewById(R.id.num_players_text))
				.setText("Set number of players: " + numPlayers);
		((TextView) findViewById(R.id.num_pictures_text))
				.setText("Set number of pictures: " + numPictures);
		((TextView) findViewById(R.id.pics_to_use_text))
				.setText("What pictures should be used?");
		takeNew = ((RadioButton) findViewById(R.id.take_new));
		takeNew.setText("Take new pictures");

		RadioButton useSavedButton = ((RadioButton) findViewById(R.id.use_saved));
		useSavedButton.setText("Use saved picture set");
		if(useSaved){
			useSavedButton.setChecked(true);
			numPicsBar.setEnabled(false);
		}

		useSavedButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				setUseSave(arg0);
			}
		});

		takeNew.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				useSaved = false;
				path = standardpath();
				numPicsBar.setEnabled(true);

			}
		});

		((Button) findViewById(R.id.next_button))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						//
						if (useSaved) {
							finishGameSetup();
							return;
						}
						Intent intent = new Intent(GameSetup.this,
								CameraActivity.class);
						intent.putExtra("numPictures", numPictures);
						intent.putExtra("path", path);
						startActivityForResult(intent, TAKEPICRESULT); //

					}

				});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt("numPlayers", numPlayers);
		outState.putInt("numPictures", numPictures);
		outState.putString("path", path);
		outState.putBoolean("useSaved", useSaved);
	}

	/**
	 * This ends and returns the game setup the the main activity
	 */
	private void finishGameSetup() {
		Intent intent = new Intent();
		intent.putExtra("gamesetup", new GameSetupCont(numPlayers, numPictures,
				path));
		// intent.putExtra("hello", "Hello");
		setResult(RESULT_OK, intent);

		finish();
	}

	/**
	 * If the camera result is ok the game setup is finished
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK)
			switch (requestCode) {
			case TAKEPICRESULT:
				finishGameSetup();
			}
	}

	/**
	 * This method is called if the user is trying to select the "used saved picture" option
	 * @param arg0
	 */
	private void setUseSave(View arg0) {
		PopupMenu popup = new PopupMenu(GameSetup.this, arg0);
		popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());

		final File files[] = MainActivity.pathSavedPics.listFiles();

		if (files.length == 0) {
			takeNew.setChecked(true);
			print("There are no saved pictures\nPlease create a new set from main menu settings");
			return;
		}

		for (int i = 0; i < files.length; i++) {
			popup.getMenu().add(0, i, 0, files[i].getName());
		}

		//Show a popup of the existing sets
		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				useSaved = true;
				File choosen = files[item.getItemId()];
				numPictures = choosen.list().length;
				numPicsBar.setEnabled(false);

				((TextView) findViewById(R.id.num_pictures_text))
						.setText("Set number of pictures: " + numPictures);

				path = choosen.getAbsolutePath();

				return true;
			}

		});

		popup.show();
	}

	/**
	 * This specifies the functions from the seekbars
	 */
	@Override
	public void onProgressChanged(SeekBar bar, int arg1, boolean arg2) {
		switch (bar.getId()) {

		case R.id.num_pictures_bar: {
			numPictures = (int) (Math.round(2 + arg1
					* (MainActivity.MAX_PICTURES - 2) * 1.0 / 100));

			((TextView) findViewById(R.id.num_pictures_text))
					.setText("Set number of pictures: " + numPictures);
			break;
		}
		case R.id.num_players_bar: {
			numPlayers = (int) (Math.round(1 + arg1
					* (MainActivity.MAX_PLAYERS - 1) * 1.0 / 100));

			((TextView) findViewById(R.id.num_players_text))
					.setText("Set number of players: " + numPlayers);

			break;
		}
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {

	}

	/**
	 * The standard path for a game
	 * @return
	 */
	private String standardpath() {
		return MainActivity.tempFolder.getPath();
	}

	public void print(String s) {
		final Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
	}
}
