package com.matslexell.photomemory;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class handles the turntable, ie who's turn it is and the score of that
 * player.
 * 
 * @author matslexell
 * 
 */
public class TurnTable implements Parcelable {
	private int turn = 0;
	private int playerScore[];
	private int totalTurns = 0;

	public static final Parcelable.Creator<TurnTable> CREATOR = new Parcelable.Creator<TurnTable>() {
		public TurnTable createFromParcel(Parcel source) {
			return new TurnTable(source);
		}

		@Override
		public TurnTable[] newArray(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

	};

	public TurnTable(int numPlayers) {
		playerScore = new int[numPlayers];
	}

	private TurnTable(Parcel source) {
		source.readIntArray(playerScore);
		turn = source.readInt();
	}

	/**
	 * This recreates the turntable object from a data string.
	 * 
	 * @param data
	 */
	public TurnTable(String data) {
		String array[] = data.split("\n");
		int pos = 0;
		int size = Integer.parseInt(array[pos]);
		pos++;
		playerScore = new int[size];
		for (int i = 0; i < size; i++, pos++)
			playerScore[i] = Integer.parseInt(array[pos]);

		turn = Integer.parseInt(array[pos]);
		pos++;
		totalTurns = Integer.parseInt(array[pos]);
	}

	/**
	 * Next turn!
	 */
	public void flipTurn() {
		totalTurns++;
		turn = (turn + 1) % playerScore.length;
	}

	/**
	 * Add a score to the player who's turn it is
	 */
	public void addScore() {
		playerScore[turn]++;
	}

	public int currentPlayerScore() {
		return playerScore[turn];
	}

	public String currentPlayerTurn() {
		return "Player " + (turn + 1);
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * A string containing the game result
	 * 
	 * @return
	 */
	public String result() {
		String str = "";
		for (int i = 0; i < playerScore.length; i++) {
			str += "Player " + (i + 1) + ": " + playerScore[i] + "\n";
		}
		str += "\nTotal turns: " + totalTurns + "\n";
		return str;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeIntArray(playerScore);
		arg0.writeInt(turn);
		// TODO Auto-generated method stub

	}

	/**
	 * For testing only
	 */
	public String toString() {
		String str = "Turn: " + turn + "\n";
		for (int i = 0; i < playerScore.length; i++) {
			str += " Player " + (i + 1) + ", ";
		}
		return str;
	}

	/**
	 * Recreates the object to a data string. This can be saved to a file and
	 * reloaded as an object.
	 * 
	 * @return
	 */
	public String toData() {
		String data = playerScore.length + "\n";
		for (int i : playerScore)
			data += i + "\n";

		data += turn + "\n";
		data += totalTurns + "\n";
		return data;
	}
}
