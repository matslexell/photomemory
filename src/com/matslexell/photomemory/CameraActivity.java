package com.matslexell.photomemory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This class handles the camera.
 * 
 * @author matslexell
 * 
 */
public class CameraActivity extends Activity {
	private static final String TAG = "CameraActivity";
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	private Camera mCamera;
	private CameraPreview mPreview;
	private FrameLayout preview;
	private int nr = 0;
	private int numPictures;
	private String path;

	// Picture callback takes the pictures
	private PictureCallback mPicture = new PictureCallback() {

		/**
		 * Picture callback handles saving the pictures to file and checking if
		 * all the pictures are taken
		 */
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			nr++;
			File pictureFile = new File(path + File.separator + nr + ".jpg");

			TextView text = (TextView) findViewById(R.id.textView1);
			text.setText(nr + "/" + numPictures + " taken");
			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d(TAG, "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d(TAG, "Error accessing file: " + e.getMessage());
			}
			
			//If all pictures are taken, then return
			if (nr == numPictures) {
				releaseCamera();
				setResult(RESULT_OK, new Intent());
				finish();
				return;
			}
			mCamera.startPreview();

		}

	};

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_activity);
		
		//Read the input variables
		numPictures = getIntent().getExtras().getInt("numPictures");
		path = getIntent().getExtras().getString("path");
		
		TextView text = (TextView) findViewById(R.id.textView1);
		text.setText(nr + "/" + numPictures + " taken");

		// Add a listener to the Capture button
		ImageView captureButton = (ImageView) findViewById(R.id.button_capture);
		captureButton.setOnClickListener(new View.OnClickListener() {
			
			/**
			 * On click: Take a picture
			 */
			@Override
			public void onClick(View v) {

				mCamera.takePicture(null, null, mPicture);
			}

		});

		// Create an instance of Camera
		mCamera = getCameraInstance();

		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera);
		preview = (FrameLayout) findViewById(R.id.camera_preview);
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) preview
				.getLayoutParams();

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		params.width = metrics.widthPixels; // TODO fix
		params.height = params.width;
		preview.setLayoutParams(params);
		preview.addView(mPreview);

		/**
		 * This orientation listener makes sure that the pictures are rotated accordingly.
		 */
		OrientationEventListener myOrientationListener = new OrientationEventListener(
				this) {
			double prev = 0;

			@Override
			public void onOrientationChanged(int arg0) {
				// double angle = Math.sin(arg0 * 2*Math.PI /360);
				if (mCamera == null)
					return;
				double angle = Math.abs((arg0 + 180) % 360) - 180;
				if (Math.abs(prev - angle) > 70) {

					prev = Math.round(angle / 90) * 90;

					Parameters params = mCamera.getParameters();
					params.setRotation((int) (90 + prev));
					mCamera.setParameters(params);

				}
			}

		};
		myOrientationListener.enable();

	}

	/**
	 * This method returns the most square-ish photo. Suitable for this application.
	 * @param camera
	 * @return
	 */
	public Size getSuitableSize(Camera camera) {
		Parameters params = camera.getParameters();
		List<Size> sizes = params.getSupportedPictureSizes();

		double max = 0;
		Size result = null;

		for (Size size : sizes) {
			double ratio = size.height * 1.0 / size.width;
			ratio = ratio <= 1 ? ratio : 1.0 / ratio;
			if (ratio > max) {
				result = size;
				max = ratio;
			}
		}

		print(result.height + ", " + result.width);

		return result;
	}

	@Override
	public void onResume() {
		super.onResume();
		// print("onResume");
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed(); // optional depending on your needs
		setResult(RESULT_CANCELED, new Intent());
		finish();
	}

	@Override
	protected void onPause() {
		super.onPause();
		releaseCamera(); // release the camera immediately on pause event
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	/**
	 * Prints the input string
	 * @param s
	 */
	public void print(String s) {
		Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
	}

}
