package com.matslexell.photomemory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

/**
 * This class handles the algorithms for loading pictures into the game frame.
 * It also handles the information on which pictures have been clicked on, found
 * and which ones that are similar.
 * 
 * @author matslexell
 * 
 */
public class Pictures implements Parcelable {
	final private int FOUND_ICON = R.drawable.abc_ab_bottom_solid_light_holo;
	final private int HIDDEN_ICON = R.drawable.abc_ab_bottom_solid_dark_holo;

	// Has the player recently clicked an image?
	private boolean clicked = false;

	/*
	 * All pictures gets stored with an integer value (from 1.jpg to n.jpg).
	 * This is a shuffled array of these numbers that are used to load pictures
	 * into position
	 */
	private int[] fileValue;

	/*
	 * All squares has a unique id as well as an id for an image. There are
	 * always two squares that has an image id in common for each image id.
	 */
	private int prevUniqueId = -1;
	private int prevImageId = -1;

	// This is used for when to images are clicked but no match is found.
	private int noMatchId = -1;

	// Array with all the already found images
	private ArrayList<Integer> foundList = new ArrayList<Integer>();

	private ImageView[] images;
	private GameActivity activity;

	public Pictures(int num) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		for (int i = 0; i < num; i++) {
			al.add(i);
			al.add(i);
		}

		Collections.shuffle(al);
		fileValue = listToGeneric(al);
	}

	private Pictures(Parcel source) {
		source.readIntArray(fileValue);
		int fList[] = null;
		source.readIntArray(fList);
		foundList = genericToList(fList);

		clicked = 1 == source.readInt();
		prevUniqueId = source.readInt();
		prevImageId = source.readInt();
		noMatchId = source.readInt();
	}

	/**
	 * This recreates an instance from a string representation of the data.
	 */
	public Pictures(String data) {

		String array[] = data.split("\n");
		int pos = 0;
		int size = Integer.parseInt(array[pos]);
		pos++;
		fileValue = new int[size];
		for (int i = 0; i < size; i++, pos++)
			fileValue[i] = Integer.parseInt(array[pos]);

		size = Integer.parseInt(array[pos]);
		pos++;

		for (int i = 0; i < size; i++, pos++)
			foundList.add(Integer.parseInt(array[pos]));

		clicked = array[pos].equals("1");
		pos++;

		prevUniqueId = Integer.parseInt(array[pos]);
		pos++;

		prevImageId = Integer.parseInt(array[pos]);
		pos++;

		noMatchId = Integer.parseInt(array[pos]);
	}

	/**
	 * If all pictures are found, game is over!
	 * 
	 * @return
	 */
	private boolean gameOver() {
		return foundList.size() == fileValue.length / 2;
	}

	/**
	 * This method is called when a square is clicked by the user. What happens
	 * is strictly dependent on the previous state of the game.
	 * 
	 * @param bm
	 * @param imageId
	 * @param uniqueId
	 */
	public void whenClicked(Bitmap bm, int imageId, int uniqueId) {
		// If the noMatchId id is set, then previous choice generated no
		// result. Hide the pieces and reset!
		if (noMatchId != -1) {
			images[noMatchId].setImageResource(HIDDEN_ICON);
			images[prevUniqueId].setImageResource(HIDDEN_ICON);
			noMatchId = prevUniqueId = prevImageId = -1;
		}

		// If a found item is clicked, do nothing
		if (foundList.contains(imageId))
			return;

		// If the same image is clicked, do nothing
		if (uniqueId == prevUniqueId)
			return;

		images[uniqueId].setImageBitmap(bm);

		// if a previous click was made, check if a pair has been found
		if (clicked) {
			if (imageId == prevImageId) {
				foundList.add(imageId);
				images[uniqueId].setImageResource(FOUND_ICON);
				images[prevUniqueId].setImageResource(FOUND_ICON);
				noMatchId = prevUniqueId = prevImageId = -1;
				activity.addScore();
				if (gameOver()) {
					activity.gameOver();
					return;
				}

				activity.print("    You found a pair!    \nNow it's still your turn.");
			} else {
				noMatchId = prevUniqueId;
				activity.print("       Sorry, no match       \nClick any picture to continue");
				activity.flipTurn();
			}
		}

		// The clicked boolean is flipped
		clicked = !clicked;
		prevUniqueId = uniqueId;
		prevImageId = imageId;
	}

	public static final Parcelable.Creator<Pictures> CREATOR = new Parcelable.Creator<Pictures>() {
		public Pictures createFromParcel(Parcel source) {
			return new Pictures(source);
		}

		@Override
		public Pictures[] newArray(int arg0) {
			return null;
		}

	};

	/**
	 * This method creates the playing field.
	 * 
	 * @param context
	 * @param frame
	 * @param path
	 */
	public void createField(GameActivity context, FrameLayout frame, String path) {
		activity = context;

		// Read the files from the picture set
		File file[] = new File(path).listFiles();

		// Calculate the alignment for the pictures
		Alignment align = maxSize(fileValue.length, frame.getHeight(),
				frame.getWidth());
		images = new ImageView[fileValue.length];

		// Load all the images!
		for (int j = 0; j < fileValue.length; j++) {

			ImageView img = new ImageView(context);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = BitmapFactory.decodeFile(
					file[fileValue[j]].getAbsolutePath(), options);
			
			final Bitmap imgBitmap = Bitmap.createScaledBitmap(bitmap,
					align.maxsize, align.maxsize, false);

			final int imgId = fileValue[j];
			final int uniId = j;

			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			
			//This sets the picture to the appropriate image depending on the state 
			if (foundList.contains(fileValue[j]))
				img.setImageResource(FOUND_ICON);
			else if (uniId == prevUniqueId || uniId == noMatchId)
				img.setImageBitmap(imgBitmap);
			else
				img.setImageResource(HIDDEN_ICON);

			//Sets the picture alignment
			params.height = align.maxsize - 2;
			params.width = align.maxsize - 2;
			params.topMargin = align.maxsize *  (j / align.col);
			params.leftMargin = align.maxsize * (j % align.col);

			img.setLayoutParams(params);
			//Add actionlistener
			img.setOnClickListener(new View.OnClickListener() {
				final Bitmap bitmap = imgBitmap;
				final int imageId = imgId;
				final int uniqueId = uniId;

				@Override
				public void onClick(View v) {
					whenClicked(bitmap, imageId, uniqueId);
				}
			});

			images[j] = img;
			//Finally add view to frame
			frame.addView(img);

		}

	}

	/**
	 * This algorithm calculates the maxsize that a square containing a picture
	 * can have in the frame. The input is the number of pictures and the height and width of the frae
	 * 
	 * @param num
	 * @param height
	 * @param width
	 * @return
	 */
	private Alignment maxSize(int num, int height, int width) {
		Alignment alignment = new Alignment();
		
		for (int row = 1; row < num; row++) {
			for (int col = 1; col < num; col++) {
				if (row * col >= num) {
					int size = Math.min(height / row, width / col);
					if (size > alignment.maxsize) {
						alignment.maxsize = size;
						alignment.col = col;

					}
				}
			}
		}

		return alignment;
	}

	// Container for alignment, only used in "maxSize" and "createField"
	class Alignment {
		// int h;
		int col;
		int maxsize;
	}

	/**
	 * Converts an ArrayList<Integer> to a generic int array.
	 * 
	 * @param al
	 * @return
	 */
	private int[] listToGeneric(ArrayList<Integer> al) {
		int generic[] = new int[al.size()];
		for (int i = 0; i < al.size(); i++)
			generic[i] = al.get(i);
		return generic;
	}

	/**
	 * Converts a generic int array into an ArrayList<Integer>
	 * 
	 * @param generic
	 * @return
	 */
	private ArrayList<Integer> genericToList(int[] generic) {
		ArrayList<Integer> al = new ArrayList<Integer>(generic.length);
		for (int i : generic)
			al.add(i);
		return al;
	}

	@Override
	public int describeContents() {

		return 0;
	}

	/**
	 * To String... only used for testing
	 */
	public String toString() {
		return "Clicked" + clicked + ", " + genericToList(fileValue).toString()
				+ ", prevUniqueId: " + prevUniqueId + ", prevImageId: "
				+ prevImageId + ", noMatchId: " + noMatchId + " FOUND: "
				+ foundList.toString();

	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeIntArray(fileValue);
		arg0.writeIntArray(listToGeneric(foundList));
		arg0.writeInt(clicked ? 1 : 0);
		arg0.writeInt(prevUniqueId);
		arg0.writeInt(prevImageId);
		arg0.writeInt(noMatchId);
	}

	/**
	 * Creates a string data representation of this object, that can be loaded
	 * for recreation.
	 * 
	 * @return
	 */
	public String toData() {
		String str = fileValue.length + "\n";
		for (int v : fileValue)
			str += v + "\n";

		str += foundList.size() + "\n";
		for (int f : foundList)
			str += f + "\n";

		str += (clicked ? "1" : "0") + "\n";

		str += prevUniqueId + "\n";
		str += prevImageId + "\n";
		str += noMatchId + "\n";

		return str;
	}

}
