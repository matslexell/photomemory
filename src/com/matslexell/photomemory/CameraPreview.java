package com.matslexell.photomemory;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * A basic Camera preview class. This class is based on the tutorials from the
 * android developer sites
 */
public class CameraPreview extends SurfaceView implements
		SurfaceHolder.Callback {
	private static final String TAG = "CameraPreview";

	private SurfaceHolder mHolder;
	private Camera mCamera;
	private CameraActivity context;

	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;

		this.context = (CameraActivity) context;

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, now tell the camera where to draw the
		// preview.
		try {
			mCamera.setPreviewDisplay(holder);
			mCamera.startPreview();
		} catch (IOException e) {
			Log.d(TAG, "Error setting camera preview: " + e.getMessage());
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// empty. Take care of releasing the Camera preview in your activity.

	}

	public void setCamera(Camera mCamera) {
		this.mCamera = mCamera;
	}

	public Size getSuitableSize(Camera camera) {
		Parameters params = camera.getParameters();
		List<Size> sizes = params.getSupportedPictureSizes();

		double max = 0;
		Size result = null;

		for (Size size : sizes) {
			double ratio = size.height * 1.0 / size.width;
			ratio = ratio <= 1 ? ratio : 1.0 / ratio;
			if (ratio > max) {
				result = size;
				max = ratio;
			}
		}

		return result;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.

		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}

		// set preview size and make any resize, rotate or
		// reformatting changes here
		mCamera.setDisplayOrientation(90);

		Size size = getSuitableSize(mCamera);

		Parameters params = mCamera.getParameters();
		params.setPictureSize(size.width, size.height);
		params.setRotation(90);
		mCamera.setParameters(params);

		params.setPreviewSize(size.width, size.height);

		// start preview with new settings

		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();

		} catch (Exception e) {
			Log.d(TAG, "Error starting camera preview: " + e.getMessage());
		}
	}
}
