package com.matslexell.photomemory;

import java.io.File;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * This activity is used for when creating a new custom picture set.
 * 
 * @author matslexell
 * 
 */
public class CreateSet extends ActionBarActivity implements
		OnSeekBarChangeListener {
	private static final int TAKEPICRESULT = 0;
	private int numPictures = 2;
	private String name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.createset_activity);

		// Seekbar for setting amunt of pictures
		SeekBar numPicsBar = (SeekBar) findViewById(R.id.nr_pictures_bar);
		numPicsBar.setOnSeekBarChangeListener(this);

		((TextView) findViewById(R.id.num_pictures_text))
				.setText("Set number of pictures: " + numPictures);

		final EditText setNameField = (EditText) findViewById(R.id.setname_field);
		if (name != null)
			setNameField.setText(name);

		// Set name of textfield

		// When name ant amount is set this button is clicked
		Button takePics = (Button) findViewById(R.id.takepics);
		takePics.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				File pathSavedPics = MainActivity.pathSavedPics;
				name = setNameField.getText().toString();

				// Examine if the name is already taken
				File folder = new File(pathSavedPics + File.separator + name);
				if (folder.exists()) {
					print("A picture set with that name already exists, please choose another...");
					return;
				}

				// Create the folder for the pics to be stored in
				if (!folder.mkdirs()) {
					Log.d(folder.getAbsolutePath(),
							"failed to create directory");
				}

				// Start activity for taking pictures
				Intent intent = new Intent(CreateSet.this, CameraActivity.class);
				intent.putExtra("numPictures", numPictures);
				intent.putExtra("path", folder.getPath());
				startActivityForResult(intent, TAKEPICRESULT); //

			}
		});

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("numPictures", numPictures);
		outState.putString("name", name);
	}

	/**
	 * Waits for the result from taking pictures. Finish if all pictures are
	 * taken. Stay if they are not and delete the folder since it might not be
	 * used.
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == TAKEPICRESULT) {
				Intent intent = new Intent();
				intent.putExtra("name", name);
				setResult(RESULT_OK, intent);

				finish();
			}
		} else if (resultCode == RESULT_CANCELED) {

			File folder = new File(MainActivity.pathSavedPics + File.separator
					+ name);
			MainActivity.delete(folder);

		}

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed(); // optional depending on your needs
		setResult(RESULT_CANCELED, new Intent());
	}

	/**
	 * Restores after on create
	 */
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {

		super.onRestoreInstanceState(savedInstanceState);

		numPictures = savedInstanceState.getInt("numPictures");
		name = savedInstanceState.getString("name");

	}

	/**
	 * Handles seekbar changes
	 */
	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
		numPictures = (int) (Math.round(2 + arg1
				* (MainActivity.MAX_PICTURES - 2) * 1.0 / 100));

		((TextView) findViewById(R.id.num_pictures_text))
				.setText("Set number of pictures: " + numPictures);
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {

	}

	public void print(String s) {
		final Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
	}

}
