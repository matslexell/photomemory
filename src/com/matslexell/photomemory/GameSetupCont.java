package com.matslexell.photomemory;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Toast;

/**
 * This is a container that holds the data for a game setup
 * 
 * @author matslexell
 * 
 */
public class GameSetupCont implements Parcelable {

	private int numPlayers;
	private int numPictures;
	private String path;

	// Is the game resumed?
	private boolean resume = false;

	private Pictures pictures;
	private TurnTable turnTable;

	public int getNumPlayers() {
		return numPlayers;
	}

	public int getNumPictures() {
		return numPictures;
	}

	public String getPath() {
		return path;
	}

	public GameSetupCont(int numPlayers, int numPictures, String path) {
		this.numPlayers = numPlayers;
		this.numPictures = numPictures;
		this.path = path;
	}

	public GameSetupCont(Pictures pictures, TurnTable turnTable, String path) {
		this.pictures = pictures;
		this.turnTable = turnTable;
		resume = true;
		this.path = path;
	}

	public static final Parcelable.Creator<GameSetupCont> CREATOR = new Parcelable.Creator<GameSetupCont>() {
		public GameSetupCont createFromParcel(Parcel source) {

			// If readInt == 1, then the game is resumed!
			if (source.readInt() == 1)
				return new GameSetupCont(new Pictures(source.readString()),
						new TurnTable(source.readString()), source.readString());

			return new GameSetupCont(source.readInt(), source.readInt(),
					source.readString());
		}

		@Override
		public GameSetupCont[] newArray(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

	};

	public boolean resume() {
		return resume;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(resume() ? 1 : 0);
		if (resume()) {
			arg0.writeString(pictures.toData());
			arg0.writeString(turnTable.toData());
		} else {
			arg0.writeInt(numPlayers);
			arg0.writeInt(numPictures);
		}
		arg0.writeString(path);

	}

	public Pictures getPictures() {
		return pictures;
	}

	public TurnTable getTurnTable() {
		return turnTable;
	}

	/**
	 * Only for testing
	 */
	public String toString() {
		String str = pictures == null ? "no pic" : pictures.toData();
		str += "\n";
		str += turnTable == null ? "no turnTable" : turnTable.toData();
		str += "\n";
		str += path;

		return str;
	}
}
