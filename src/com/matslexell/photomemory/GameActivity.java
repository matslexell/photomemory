package com.matslexell.photomemory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

/**
 * This class is used when in game!
 * 
 * @author matslexell
 * 
 */
public class GameActivity extends ActionBarActivity {

	// Picture handler
	private Pictures pictures;

	// Turntable handler
	private TurnTable turnTable;

	// Path to pictures
	private String path;

	private boolean gameover = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		// If the game is started or resumed
		if (savedInstanceState == null) {
			// Get game setup data
			GameSetupCont gameSetupCont = (GameSetupCont) getIntent()
					.getExtras().getParcelable("gamesetup");
			// If thegame is resumed the piture and
			if (gameSetupCont.resume()) {
				pictures = gameSetupCont.getPictures();
				turnTable = gameSetupCont.getTurnTable();

			} else {
				pictures = new Pictures(gameSetupCont.getNumPictures());
				turnTable = new TurnTable(gameSetupCont.getNumPlayers());
			}
			path = gameSetupCont.getPath();
		} else {
			// Else recreate game
			pictures = (Pictures) savedInstanceState.getParcelable("pictures");
			turnTable = (TurnTable) savedInstanceState
					.getParcelable("turnTable");
			path = savedInstanceState.getString("path");
		}

		draw();
		updateActionBar();

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelable("pictures", pictures);
		outState.putParcelable("turnTable", turnTable);
		outState.putString("path", path);
		// No need to save gameover variable
	}

	/**
	 * This method "draws" the game field. Ie, loads the pictures according to
	 * their corresponding state
	 */
	public void draw() {
		final FrameLayout frame = (FrameLayout) findViewById(R.id.game_frame);
		final GameActivity context = this;

		// This is necessary to get the exact measurements of the frame. When
		// layout is created the pictures are loaded
		frame.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					@SuppressLint("NewApi")
					@Override
					public void onGlobalLayout() {
						frame.getViewTreeObserver()
								.removeOnGlobalLayoutListener(this);
						pictures.createField(context, frame, path);
					}
				});
	}

	/**
	 * This updates the information of playerturn and score
	 */
	private void updateActionBar() {
		android.app.ActionBar ab = getActionBar();

		ab.setTitle(turnTable.currentPlayerTurn() + "'s Turn");
		ab.setSubtitle("Score: " + turnTable.currentPlayerScore());

	}

	/**
	 * When game is abruptly stopped, but not yet finished, it's state is being saved to file so it can be restarted later
	 */
	@Override
	protected void onStop() {
		super.onStop();
		if (gameover)
			return;

		TextIO picFile = new TextIO(MainActivity.picTemp.getPath(), true);
		TextIO turnFile = new TextIO(MainActivity.turnTemp.getPath(), true);
		TextIO pathFile = new TextIO(MainActivity.pathTemp.getPath(), true);

		picFile.print(pictures.toData());
		turnFile.print(turnTable.toData());
		pathFile.print(path);

	}

	/**
	 * Prints the game messages
	 * @param s
	 */
	public void print(String s) {
		final Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
		toast.show();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				toast.cancel();
			}
		}, 1500);
	}

	
	public void addScore() {
		turnTable.addScore();
		updateActionBar();
	}

	public void flipTurn() {
		turnTable.flipTurn();
		updateActionBar();

	}

	public void gameOver() {
		gameover = true;
		Intent intent = new Intent();
		intent.putExtra("result", turnTable.result());
		setResult(RESULT_OK, intent);
		finish();
	}

}
