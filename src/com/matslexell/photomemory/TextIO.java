package com.matslexell.photomemory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
/**
 * This class handles reading text from a file.
 * @author matslexell
 *
 */
public class TextIO {

	private File file;
	
	private String encoding = "UTF-8";
	
	public TextIO(String path) {
		file = new File(path);
	}
	
	public TextIO(String path, boolean delete) {
		file = new File(path);
		if(delete)
			delete();
	}
	
	/***
	 * Sets encoding to the inputted encoding.
	 * @param encoding
	 */
	public void setEncoding(String encoding){
		this.encoding = encoding;
	}
	
	/**
	 * Always returns the first line of the document (and then closes the readers).
	 * @return
	 */
	public String getLine() {
		try {
			InputStream is = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(is,
					encoding));
			String line = br.readLine();
			br.close();
			return line;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * This delets the input file from the computer harddrive. Be carefull, no turning back!
	 */
	public void delete(){
		file.delete();
	}
		
	/***
	 * Appends the input string to the end of the file.
	 * @param string
	 */
	public void print(String string) {
		try {
			FileOutputStream fos = new FileOutputStream(file, true);
			OutputStreamWriter out = new OutputStreamWriter(fos, encoding);

			//Another possible way is to use PrintWriter...
//			PrintWriter out = new PrintWriter(new FileOutputStream(path, true));
			
			out.write(string);
			out.close();
		} catch (IOException e) {
			// oh noes!
			e.printStackTrace();
		}
	}
	
	/***
	 * Appends the input string to the end of the file and adds a new line.
	 * @param string
	 */
	public void println(String string) {
		print(string + "\n");
	}
	
	/***
	 * Returns the entire text of the file.
	 * @param string
	 */
	public String getEntireText() {
		String string = null;
		try {
			string = new Scanner(file).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return string;
	}
}
