package com.matslexell.photomemory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.MenuItem;
/**
 * This activity shows information about the game
 * @author matslexell
 *
 */
public class About extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
        getActionBar().setDisplayHomeAsUpEnabled(true);

		

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if (item.getItemId() == android.R.id.home) {
	    	 finish();
	    	 return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
